import numpy as np


def rastrigin(X, Y, dims):
    A = 10
    Z = np.sum(X ** 2 - A * np.cos(2 * np.pi * X)) + \
        np.sum(Y ** 2 - A * np.cos(2 * np.pi * Y)) + A * dims
    return Z


def create_search_space(f, bounds, search_space_dims, dims):
    xaxis = np.linspace(bounds[0], bounds[1], search_space_dims[0])
    yaxis = np.linspace(bounds[0], bounds[1], search_space_dims[1])
    X, Y = np.meshgrid(xaxis, yaxis)
    search_space = np.array([f(x, y, dims) for (x, y) in zip(np.ravel(X), np.ravel(Y))]).reshape(search_space_dims)
    return search_space


def create_simple_search_space(score_range, search_space_dims):
    rng = np.random.default_rng()
    search_space = rng.uniform(score_range[0], score_range[1], (search_space_dims))
    return search_space