from utils import create_search_space as cspace
from utils import rastrigin as rg
from GridSearchOptimizer import optimize_by_search_space_size, optimize_by_n_agents, \
  optimize_by_n_rogue_agents, optimize_by_noise, optimize_by_n_visits, optimize_by_consensus_rounds, \
  optimize_by_majority_needed,optimize_n_visits_vs_rogue, optimize_by_n_neighbor, \
  optimize_n_visits_vs_n_neighbors,optimize_majority_vs_n_consensus

# properties of swarm
n_agents = 15
n_visits = 3
n_dimensions = 2
n_neighbors = 2
n_consensus = 3
majority_threshold = 0.5

# properties of function
search_space_dims = [(5, 5), (11, 11), (17, 15), (21, 21), (31, 31), (101, 101), (301, 301)]
search_space_functions = [(rg, (-5.12,5.12))]

# properties of rogue agents in swarm
n_rogue_agents = 0

# properties of noise
noise = 0.

# create search space matrix
search_spaces = []
for function, bounds in search_space_functions:
  for dimension in search_space_dims:
    search_spaces.append(
      cspace(
        function,
        bounds=bounds,
        search_space_dims=dimension,
        dims=2
      )
    )

index_of_961 = 4

# accuracy and number of iterations against size of search space
runs = 10
optimize_by_search_space_size(n_agents, n_visits, n_neighbors, n_consensus, majority_threshold, search_spaces, runs)

# accuracy and number of iterations against number of agents
n_agents = [5, 10, 15, 30, 100]
optimize_by_n_agents(n_agents, n_visits, n_neighbors, n_consensus, majority_threshold, search_spaces[index_of_961], runs)

# accuracy and number of iterations against number of rogue agents
n_agents = 15
n_rogue_agents = range(10)
# search_spaces[index_of_961]: min = 0.0, max = 77.849..
# search_spaces[index_of_961][0,0] = 57.849..
# search_spaces[index_of_961][3,13] = 33.113..
# search_spaces[index_of_961][11,15] = 18.492..
# search_spaces[index_of_961][9,9] = 9.291..
n_rogue_agent_positions = [0, 0]
optimize_by_n_rogue_agents(n_agents, n_visits, n_neighbors, n_consensus, majority_threshold, n_rogue_agents,
  n_rogue_agent_positions, search_spaces[index_of_961], runs)

n_rogue_agent_positions = [3, 13]
optimize_by_n_rogue_agents(n_agents, n_visits, n_neighbors, n_consensus, majority_threshold, n_rogue_agents,
  n_rogue_agent_positions, search_spaces[index_of_961], runs)

n_rogue_agent_positions = [11, 15]
optimize_by_n_rogue_agents(n_agents, n_visits, n_neighbors, n_consensus, majority_threshold, n_rogue_agents,
  n_rogue_agent_positions, search_spaces[index_of_961], runs)

n_rogue_agent_positions = [9, 9]
optimize_by_n_rogue_agents(n_agents, n_visits, n_neighbors, n_consensus, majority_threshold, n_rogue_agents,
  n_rogue_agent_positions, search_spaces[index_of_961], runs)

# how does n_visits affect the robustness of the algorithm
visits = (2,3,4,5,6,7,8)
n_rogue_agents = (1,2,3,4,5,6,7)

n_rogue_agent_positions = [11, 15]
optimize_n_visits_vs_rogue(n_rogue_agents,n_rogue_agent_positions,n_agents,visits,n_neighbors,
                           n_consensus,majority_threshold,noise,search_spaces[index_of_961],runs)

n_rogue_agent_positions = [9, 9]
optimize_n_visits_vs_rogue(n_rogue_agents,n_rogue_agent_positions,n_agents,visits,n_neighbors,
                           n_consensus,majority_threshold,noise,search_spaces[index_of_961],runs)
n_rogue_agent_positions = [3, 13]
optimize_n_visits_vs_rogue(n_rogue_agents,n_rogue_agent_positions,n_agents,visits,n_neighbors,
                           n_consensus,majority_threshold,noise,search_spaces[index_of_961],runs)

# accuracy and number of iterations against number of visits
visits = (2,3,4,5,6,7,8)
optimize_by_n_visits(n_agents, visits, n_neighbors, n_consensus, majority_threshold, noise,
                     search_spaces[index_of_961], runs)

n_consensus = (1,2,3,4,5,6,7,8)
optimize_by_consensus_rounds(n_agents,n_visits,n_neighbors,n_consensus,
                           majority_threshold,noise,search_spaces[index_of_961],runs)

# accuracy and number of iterations against amount of noise induced
amount_of_noise = (0.0,0.5,1.0,5.0,10.0,20.0,30.0)
n_consensus=3
optimize_by_noise(n_agents, n_visits, n_neighbors,amount_of_noise, n_consensus, majority_threshold, search_spaces[index_of_961], runs)

majority_threshold = (0.2,0.3,0.4,0.5,0.6,0.7)
optimize_by_majority_needed(n_agents, n_visits, n_neighbors, n_consensus, majority_threshold, noise, search_spaces[index_of_961], runs)

# effect on advertising (assume each agent makes 1 random visit -> n_visits = n_neighbor + 1)
n_neighbors = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
n_visits = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
majority_threshold = 0.5
optimize_by_n_neighbor(n_agents, n_visits, n_neighbors, n_consensus, majority_threshold, search_spaces[index_of_961], runs)
optimize_by_n_neighbor(n_agents, n_visits, n_neighbors, n_consensus, majority_threshold, search_spaces[index_of_961+2], runs)

n_visits = [3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
optimize_by_n_neighbor(n_agents, n_visits, n_neighbors, n_consensus, majority_threshold, search_spaces[index_of_961], runs)
optimize_by_n_neighbor(n_agents, n_visits, n_neighbors, n_consensus, majority_threshold, search_spaces[index_of_961+2], runs)

# neighbors agains visits
n_neighbors = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
n_additional_visits = [1, 2, 3, 4, 5, 6, 7, 8]
optimize_n_visits_vs_n_neighbors(n_agents, n_additional_visits, n_neighbors, n_consensus, majority_threshold, search_spaces[index_of_961], runs)
optimize_n_visits_vs_n_neighbors(n_agents, n_additional_visits, n_neighbors, n_consensus, majority_threshold, search_spaces[index_of_961+1], runs)
optimize_n_visits_vs_n_neighbors(n_agents, n_additional_visits, n_neighbors, n_consensus, majority_threshold, search_spaces[index_of_961+2], runs)

# majority threshold vs consensus
n_visits = 3
n_neighbors = 2
n_consensus = (1,2,3,4,5,6,7,8)
majority_threshold = (0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9)

optimize_majority_vs_n_consensus(n_agents, n_visits, n_neighbors, n_consensus, majority_threshold, search_spaces[index_of_961], runs)
