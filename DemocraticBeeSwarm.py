from attr import attrib, attrs
from attr.validators import instance_of
import numpy as np
from collections import Counter

@attrs
class DemocraticBeeSwarm(object):
  """Represents a swarm

  Attributes
  ----------
  n_agents : int
    number of agents in the swarm.
  n_visits : int
    number of positions an agent can visit per iteration.
  n_dimensions : int
    number of dimensions of the search space.
  n_neighbors : int
    number of neighbors an agent can communicate with (advertise his personal best solution).
  n_consensus : int
    number of times in a row, a global solution has to appear, to reach consensus.
  majority_threshold : float
    threshold to define majority in range :code:`[0,1)`, that a given fraction of a swarm votes for a given
    solution and this threshold is exceeded that it is a valid majority.
  bounds : tuple
    tuple of shape :code:`(low, high)` to define the lower and upper bounds of 
    the search space in each dimension respectively.
  position : numpy.ndarray
    matrix of positions of shape :code:`(n_agents, n_visits, n_dimensions)` to store the 
    positions of all agents within a given iteration.
  n_rogue_agents : int
    number of rogue agents in the swarm. They will always promote a certain position according to rogue_agent_positions.
  rogue_agent_positions : numpy.ndarray
    matrix of positions, rogue agent(s) will promote, of shape :code:`(n_rogue_agents, n_dimensions)`.
  noise : float
    noise to add to the cost of the function in the form of :code:`np.normal() * noise`.
  cost : numpy.ndarray
    matrix of costs of shape :code:`(n_agents, n_visits)` to store the costs of all agents 
    for all visits (same index `n_visits` as in position matrix) within a given iteration.
  best_position : numpy.ndarray
    matrix of best positions of shape :code:`(n_agents, n_dimensions)` to store the best position 
    (out of `n_visits` visits) for all agents respectively within a given iteration.
  global_best_solution : list
    list of global best solutions the swarm voted on for all iterations. Each item is of shape 
    :code:`(position, number_of_votes)` where position is a tuple containing coordinate for each dimension.
    A solution is only added to this list, if majority is reached (>50% voted for same).
  vote_history: list
    list of all votes (solution & number of votes) until consensus was reached.
  """
  n_agents = attrib(type=int, validator=instance_of(int))
  n_visits = attrib(type=int, validator=instance_of(int))
  n_dimensions = attrib(type=int, validator=instance_of(int))
  n_neighbors = attrib(type=int, validator=instance_of(int))
  n_consensus = attrib(type=int, validator=instance_of(int)) 
  majority_threshold = attrib(type=float, validator=instance_of(float))
  bounds = attrib(type=tuple, validator=instance_of(tuple))
  position = attrib(type=np.ndarray, validator=instance_of(np.ndarray))
  discrete = attrib(
    type=bool,
    default=False,
    validator=instance_of(bool)
  )
  n_rogue_agents = attrib(
    type=int,
    default=0,
    validator=instance_of(int)
  )
  rogue_agent_positions = attrib(
    type=np.ndarray,
    default=np.array([]),
    validator=instance_of(np.ndarray)
  )
  noise = attrib(
    type=float,
    default=0,
    validator=instance_of(float)
  )
  cost = attrib(
    type=np.ndarray,
    default=np.array([]),
    validator=instance_of(np.ndarray)
  )
  best_position = attrib(
    type=np.ndarray,
    default=np.array([]),
    validator=instance_of(np.ndarray)
  )
  global_best_solution = attrib(
    type=list,
    default=[],
    validator=instance_of(list)
  )
  vote_history = attrib(
    type=list,
    default=[],
    validator=instance_of(list)
  )
  rng = np.random.default_rng()

  def compute_cost(self, f):
    """Computes the cost of the given function and stores them in  the field `cost`.
    
    Parameters
    ----------
    f : function
      function to optimize, returns the cost
    """
    cost = [f(self.position[:, visit, :]) for visit in range(self.n_visits)]
    self.cost = np.array(cost).T

    # set cost of rogue agents to 0
    # todo: should rogue agents by randomly chosen instead of first n's?
    if self.n_rogue_agents:
      self.cost[range(self.n_rogue_agents), :] = [0] * self.n_visits
    
    # apply noise
    if self.noise:
      self.cost += self.rng.normal(size=(self.n_agents, self.n_visits)) * self.noise

  def compute_best_position(self):
    """Computes the best position for each agent and stores them in the field `best_position`."""
    idc = np.argmin(self.cost, axis=1)
    self.best_position = self.position[range(self.n_agents), idc]

  def compute_next_position(self):
    """Computes the next positions to visit in the next iteration and stores them in the field `position`."""
    # advertise best position with n_neighbors
    # todo: shuffle can lead to advertise a position to an agent itself (and not to neighbors)
    for neighbor in range(self.n_neighbors):
      self.position[:, neighbor, :] = self.best_position.copy()
      self.rng.shuffle(self.position[:, neighbor, :])

    # fill remaining position(s) with random position(s)
    low, high = self.bounds
    for visit in range(self.n_neighbors, self.n_visits):
      self.position[:, visit, :] = self._get_random_next_position()

    # set positions of rogue agents static
    if self.n_rogue_agents:
      for visit in range(self.n_visits):
        self.position[range(self.n_rogue_agents), visit, :] = self.rogue_agent_positions

  def _get_random_next_position(self):
    """Computes the next random positions (either discrete or continuous)."""
    low, high = self.bounds
    if self.discrete:
      return self.rng.integers(low=low, high=high, size=(self.n_agents, self.n_dimensions), endpoint=True)
    else:
      # todo: uniform doesn't include high itself -> [low, high) -- at the moment <hack> with round, might change
      return np.round(self.rng.uniform(low=low, high=high, size=(self.n_agents, self.n_dimensions)), 10)

  def compute_vote(self):
    """Computes the votes for each agent and if majority is met, stores the vote in the field `global_best_solution`."""
    votes = Counter([tuple(x) for x in self.best_position])

    # track vote history 
    self.vote_history.append(votes)

    # if only one solution is left -> all agents vote for this solution
    if len(votes.most_common()) == 1:
      self.global_best_solution.append(votes.most_common()[0])
      return

    # check for majority (a single solution with the most votes is needed)
    (best_solution, best_n_votes), (second_solution, second_n_votes) = votes.most_common(2)
    if best_n_votes > second_n_votes:
      # majority is for best_solution
      # check if vote threshold is also reached
      if (best_n_votes / self.n_agents) > self.majority_threshold:
        self.global_best_solution.append((best_solution, best_n_votes))
      else:
        # None indicates that in the given iteration was no majority vote
        self.global_best_solution.append((None, None))
    else:
      # no majority reached, best and second solution have same amount of votes
      self.global_best_solution.append((None, None))

  def has_reached_consensus(self):
    """Check if swarm has reached consensus.

    The same solution has to appear `n_consensus` times in a row in the field `global_best_solution`.

    Returns
    -------
    bool
      true, if consensus is reached, else false.
    """
    last_n_consensus = self.global_best_solution[-self.n_consensus:]
    # check if enough entries and not series of no votes
    if (len(last_n_consensus) >= self.n_consensus) & ((None, None) not in last_n_consensus):
      # check if all are same
      return len({best_solution for best_solution, n_votes in last_n_consensus}) == 1
    else:
      return False

def create_swarm(n_agents, n_visits, n_dimensions, n_neighbors, n_consensus, majority_threshold, bounds,
  discrete, n_rogue_agents, rogue_agent_positions, noise):
  """Creates a DemocraticBeeSwarm object
  
  See also
  --------
  DemocraticBeeSwarm : for a description of the parameters
  """
  rng =  np.random.default_rng()
  low, high = bounds

  # generate initial positions
  if discrete:
    pos = rng.integers(low=low, high=high, size=(n_agents, n_visits, n_dimensions), endpoint=True)
  else:
    # todo: uniform doesn't include high itself -> [low, high) -- at the moment <hack> with round, might change
    pos = np.round(rng.uniform(low=low, high=high, size=(n_agents, n_visits, n_dimensions)), 10)

  return DemocraticBeeSwarm(
    n_agents,
    n_visits,
    n_dimensions,
    n_neighbors,
    n_consensus,
    majority_threshold,
    bounds,
    pos,
    discrete,
    n_rogue_agents,
    rogue_agent_positions,
    noise
  )
