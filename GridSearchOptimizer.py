from DiscreteDemocraticBeeSwarm import create_swarm
import matplotlib.pyplot as plt
import numpy as np

def optimize_by_search_space_size(n_agents, n_visits, n_neighbors, n_consensus, majority_threshold, search_spaces, runs):
  """
  Parameters
  ----------
  search_spaces : list
    list of all search space matrices.
  """
  result = []
  
  # grid search for each search space matrix
  for i_search_space, search_space in enumerate(search_spaces):
    # set bounds for search space matrix
    bounds = (0, [high-1 for high in search_space.shape])

    swarms_this_run = []

    for run in range(runs):
      # create swarm
      swarm = create_swarm(n_agents, n_visits, 2, n_neighbors, n_consensus, majority_threshold, bounds,
        0, np.array([[]] * len(search_spaces)), .0)
      
      # todo: list defined with attrs do not get set to default [], instead reference is kept?!?
      swarm.global_best_solution = []
      swarm.vote_history = []

      # optimization loop
      for i in range(1000):
        # search space
        swarm.compute_cost(search_space)
        swarm.compute_best_position()

        # share best position among swarm (with n_neighbors)
        # update position (next n_visits)
        swarm.compute_next_position()

        # vote for best position
        swarm.compute_vote()

        # democratic consensus (majority n_consensus times)
        if swarm.has_reached_consensus():
          break

      # store state of swarm for a single run
      swarms_this_run.append(swarm)
      
    # store swarms for all runs
    result.append((i_search_space, swarms_this_run))

  # evaluation
  fig, ax = plt.subplots(nrows=1, ncols=2, constrained_layout=True)
  fig.suptitle("Search space size (number of agents=" + str(n_agents) + ")")
  for i_search_space, swarms in result:
    best_value_searchspace = search_spaces[i_search_space].min()
    print("Best value to find: " + str(best_value_searchspace))
    max_value_searchspace = search_spaces[i_search_space].max()

    # metrics
    iterations = [len(s.global_best_solution) for s in swarms]
    print("iterations: " + str(iterations))
    positions = [s.global_best_solution[-1] for s in swarms]
    print("positions: " + str(positions))
    solutions = np.array([search_spaces[i_search_space][tuple(pos)] for pos, n_votes in positions])
    print("solutions: " + str(solutions))
    relative_deviations = (np.abs(solutions - best_value_searchspace) / max_value_searchspace) * 100
    print("relative deviations: " + str(relative_deviations))

    # plot
    ax[0].boxplot(relative_deviations, positions=[i_search_space], labels=[search_spaces[i_search_space].size])
    ax[1].boxplot(iterations, positions=[i_search_space], labels=[search_spaces[i_search_space].size])

  ax[0].set_xlabel('Search space size')
  ax[0].set_ylabel('Relative deviation to best solution [%]')
  ax[1].set_xlabel('Search space size')
  ax[1].set_ylabel('Number of iterations')
  for ax in fig.axes:
    plt.sca(ax)
    plt.xticks(rotation=45)
  plt.savefig("./paper/Figures/plots/searchspace_optimization.pdf", format = 'pdf')

def optimize_by_n_agents(n_agents, n_visits, n_neighbors, n_consensus, majority_threshold, search_space, runs):
  result = []
  
  # grid search for each number of agents
  for n_agent in n_agents:
    # set bounds for search space matrix
    bounds = (0, [high-1 for high in search_space.shape])

    swarms_this_run = []

    for run in range(runs):
      # create swarm
      swarm = create_swarm(n_agent, n_visits, 2, n_neighbors, n_consensus, majority_threshold, bounds,
        0, np.array([]), .0)
      
      # todo: list defined with attrs do not get set to default [], instead reference is kept?!?
      swarm.global_best_solution = []
      swarm.vote_history = []

      # optimization loop
      for i in range(1000):
        # search space
        swarm.compute_cost(search_space)
        swarm.compute_best_position()

        # share best position among swarm (with n_neighbors)
        # update position (next n_visits)
        swarm.compute_next_position()

        # vote for best position
        swarm.compute_vote()

        # democratic consensus (majority n_consensus times)
        if swarm.has_reached_consensus():
          break

      # store state of swarm for a single run
      swarms_this_run.append(swarm)
      
    # store swarms for all runs
    result.append((n_agent, swarms_this_run))

  # evaluation
  fig, ax = plt.subplots(nrows=1, ncols=2, constrained_layout=True)
  fig.suptitle("Number of agents (search space size=" + str(search_space.size) + ")")

  best_value_searchspace = search_space.min()
  print("Best value to find: " + str(best_value_searchspace))
  max_value_searchspace = search_space.max()

  for i, (i_agent, swarms) in enumerate(result):
    # metrics
    iterations = [len(s.global_best_solution) for s in swarms]
    print("iterations: " + str(iterations))
    positions = [s.global_best_solution[-1] for s in swarms]
    print("positions: " + str(positions))
    solutions = np.array([search_space[tuple(pos)] for pos, n_votes in positions if pos != None])
    print("solutions: " + str(solutions))
    relative_deviations = (np.abs(solutions - best_value_searchspace) / max_value_searchspace) * 100
    print("relative deviations: " + str(relative_deviations))

    # plot
    ax[0].boxplot(relative_deviations, positions=[i], labels=[i_agent])
    ax[1].boxplot(iterations, positions=[i], labels=[i_agent])

  ax[0].set_xlabel('Number of agents')
  ax[0].set_ylabel('Relative deviation to best solution [%]')
  ax[1].set_xlabel('Number of agents')
  ax[1].set_ylabel('Number of iterations')
  plt.savefig("./paper/Figures/plots/number_of_agents_optimization.pdf", format = 'pdf')

def optimize_by_n_rogue_agents(n_agents, n_visits, n_neighbors, n_consensus, majority_threshold, n_rogue_agents, 
  n_rogue_agent_positions, search_space, runs):
  result = []
  
  # grid search for each number of agents
  for i, n_rogue_agent in enumerate(n_rogue_agents):
    # set bounds for search space matrix
    bounds = (0, [high-1 for high in search_space.shape])

    swarms_this_run = []

    for run in range(runs):
      # create swarm
      swarm = create_swarm(15, n_visits, 2, n_neighbors, n_consensus, majority_threshold, bounds,
        n_rogue_agent, np.array([n_rogue_agent_positions] * n_rogue_agent), .0)
      
      # todo: list defined with attrs do not get set to default [], instead reference is kept?!?
      swarm.global_best_solution = []
      swarm.vote_history = []

      # optimization loop
      for i in range(1000):
        # search space
        swarm.compute_cost(search_space)
        swarm.compute_best_position()

        # share best position among swarm (with n_neighbors)
        # update position (next n_visits)
        swarm.compute_next_position()

        # vote for best position
        swarm.compute_vote()

        # democratic consensus (majority n_consensus times)
        if swarm.has_reached_consensus():
          break

      # store state of swarm for a single run
      swarms_this_run.append(swarm)
      
    # store swarms for all runs
    result.append((n_rogue_agent, swarms_this_run))

  # evaluation
  fig, ax = plt.subplots(nrows=1, ncols=2, constrained_layout=True)
  fig.suptitle("Number of rogue agents (search space size=" + str(search_space.size) + ",\n" +
    "number of agents=" + str(n_agents) + 
    ", quality score of rogue agents=" + str(round(search_space[tuple(n_rogue_agent_positions)], 3)) + ")")

  best_value_searchspace = search_space.min()
  print("Best value to find: " + str(best_value_searchspace))
  max_value_searchspace = search_space.max()

  for i, (i_rogue_agent, swarms) in enumerate(result):
    # metrics
    iterations = [len(s.global_best_solution) for s in swarms]
    print("iterations: " + str(iterations))
    positions = [s.global_best_solution[-1] for s in swarms]
    print("positions: " + str(positions))
    solutions = np.array([search_space[tuple(pos)] for pos, n_votes in positions if pos != None])
    print("solutions: " + str(solutions))
    relative_deviations = (np.abs(solutions - best_value_searchspace) / max_value_searchspace) * 100
    print("relative deviations: " + str(relative_deviations))

    # plot
    ax[0].boxplot(relative_deviations, positions=[i], labels=[i_rogue_agent])
    ax[1].boxplot(iterations, positions=[i], labels=[i_rogue_agent])

  ax[0].set_xlabel('Number of rogue agents')
  ax[0].set_ylabel('Relative deviation to best solution [%]')
  ax[1].set_xlabel('Number of rogue agents')
  ax[1].set_ylabel('Number of iterations')
  plt.savefig("./paper/Figures/plots/rogue_agents_{}.pdf".format(str(round(search_space[tuple(n_rogue_agent_positions)], 3))), format = 'pdf')

def optimize_by_noise(n_agents, n_visits, n_neighbors, amount_of_noise,
  n_consensus, majority_threshold, search_space, runs):

  result = []

  # grid search for each number of agents
  for noise in amount_of_noise:
    # set bounds for search space matrix
    bounds = (0, [high - 1 for high in search_space.shape])
    swarms_this_run = []
    for run in range(runs):
      # create swarm
      swarm = create_swarm(n_agents, n_visits, 2, n_neighbors, n_consensus, majority_threshold, bounds,
                           0, np.array([]), noise)

      swarm.global_best_solution = []
      swarm.vote_history = []

      # optimization loop
      for i in range(1000):
        # search space
        swarm.compute_cost(search_space)
        swarm.compute_best_position()

        # share best position among swarm (with n_neighbors)
        # update position (next n_visits)
        swarm.compute_next_position()

        # vote for best position
        swarm.compute_vote()

        # democratic consensus (majority n_consensus times)
        if swarm.has_reached_consensus():
          break

      # store state of swarm for a single run
      swarms_this_run.append(swarm)

    # store swarms for all runs
    result.append((noise, swarms_this_run))

  # evaluation
  fig, ax = plt.subplots(nrows=1, ncols=2, constrained_layout=True)
  fig.suptitle("Amount of noise (search space size=" + str(search_space.size)+ ", number of agents="+str(n_agents)+ ")")
  best_value_searchspace = search_space.min()
  print("Best value to find: " + str(best_value_searchspace))
  max_value_searchspace = search_space.max()

  for i, (i_agent, swarms) in enumerate(result):
    # metrics
    iterations = [len(s.global_best_solution) for s in swarms]
    print("iterations: " + str(iterations))
    positions = [s.global_best_solution[-1] for s in swarms]
    print("positions: " + str(positions))
    solutions = np.array([search_space[tuple(pos)] for pos, n_votes in positions if pos != None])
    print("solutions: " + str(solutions))
    relative_deviations = (np.abs(solutions - best_value_searchspace) / max_value_searchspace) * 100
    print("relative deviations: " + str(relative_deviations))

    # plot
    ax[0].boxplot(relative_deviations, positions=[i], labels=[i_agent])
    ax[1].boxplot(iterations, positions=[i], labels=[i_agent])

  ax[0].set_xlabel('Amount of noise')
  ax[0].set_ylabel('Relative deviation to best solution [%]')
  ax[1].set_xlabel('Amount of noise')
  ax[1].set_ylabel('Number of iterations')
  plt.savefig("./paper/Figures/plots/noisy_exploration.pdf", format = 'pdf')

def optimize_by_n_visits(n_agents, n_visits, n_neighbors, n_consensus,
  majority_threshold, amount_of_noise,search_space, runs):

  result = []
  # grid search for each number of agents
  for visit in n_visits:
    # set bounds for search space matrix
    bounds = (0, [high - 1 for high in search_space.shape])
    swarms_this_run = []
    for run in range(runs):
      # create swarm
      swarm = create_swarm(n_agents, visit, 2, n_neighbors, n_consensus, majority_threshold, bounds,
                           0, np.array([]), amount_of_noise)
      swarm.global_best_solution = []
      swarm.vote_history = []
      # optimization loop
      for i in range(1000):
        # search space
        swarm.compute_cost(search_space)
        swarm.compute_best_position()
        # share best position among swarm (with n_neighbors)
        # update position (next n_visits)
        swarm.compute_next_position()
        # vote for best position
        swarm.compute_vote()
        # democratic consensus (majority n_consensus times)
        if swarm.has_reached_consensus():
          break
      # store state of swarm for a single run
      swarms_this_run.append(swarm)

    # store swarms for all runs
    result.append((visit, swarms_this_run))

  # evaluation
  fig, ax = plt.subplots(nrows=1, ncols=2, constrained_layout=True)
  fig.suptitle("Number of visits (search space size=" + str(search_space.size)+ ", number of agents="+str(n_agents)+ ")")
  best_value_searchspace = search_space.min()
  print("Best value to find: " + str(best_value_searchspace))
  max_value_searchspace = search_space.max()

  for i, (i_agent, swarms) in enumerate(result):
    # metrics
    iterations = [len(s.global_best_solution) for s in swarms]
    print("iterations: " + str(iterations))
    positions = [s.global_best_solution[-1] for s in swarms]
    print("positions: " + str(positions))
    solutions = np.array([search_space[tuple(pos)] for pos, n_votes in positions if pos != None])
    print("solutions: " + str(solutions))
    relative_deviations = (np.abs(solutions - best_value_searchspace) / max_value_searchspace) * 100
    print("relative deviations: " + str(relative_deviations))

    # plot
    ax[0].boxplot(relative_deviations, positions=[i], labels=[i_agent])
    ax[1].boxplot(iterations, positions=[i], labels=[i_agent])

  ax[0].set_xlabel('Number of visits')
  ax[0].set_ylabel('Relative deviation to best solution [%]')
  ax[1].set_xlabel('Number of visits')
  ax[1].set_ylabel('Number of iterations')
  plt.savefig("./paper/Figures/plots/n_visits_optimization.pdf", format = 'pdf')

def optimize_n_visits_vs_rogue(n_rogue_agents,rogue_agents_pos,n_agents, n_visits, n_neighbors, n_consensus,
  majority_threshold, amount_of_noise,search_space, runs):

  results = []

  # grid search for each number of agents
  for n_rogue_agent in n_rogue_agents:
    result = []

    for visit in n_visits:
      # set bounds for search space matrix
      bounds = (0, [high - 1 for high in search_space.shape])
      swarms_this_run = []
      for run in range(runs):
        # create swarm
        swarm = create_swarm(n_agents, visit, 2, n_neighbors, n_consensus, majority_threshold, bounds,
                             n_rogue_agent, np.array([rogue_agents_pos] * n_rogue_agent), amount_of_noise)
        swarm.global_best_solution = []
        swarm.vote_history = []
        # optimization loop
        for i in range(1000):
          # search space
          swarm.compute_cost(search_space)
          swarm.compute_best_position()
          # share best position among swarm (with n_neighbors)
          # update position (next n_visits)
          swarm.compute_next_position()
          # vote for best position
          swarm.compute_vote()
          # democratic consensus (majority n_consensus times)
          if swarm.has_reached_consensus():
            break
        # store state of swarm for a single run
        swarms_this_run.append(swarm)

      # store swarms for all runs
      result.append((visit, swarms_this_run))
    results.append(result)
  # evaluation
  fig, ax = plt.subplots(nrows=1, ncols=2, constrained_layout=True)
  rogue_x,rogue_y = rogue_agents_pos[0],rogue_agents_pos[1]
  fig.suptitle("Number of visits (search space size=" + str(search_space.size)+ ", number of agents="+str(n_agents)+ ",\n"+
               "quality score of rogue agents="+str(round(search_space[rogue_x,rogue_y], 3))+")")
  best_value_searchspace = search_space.min()
  print("Best value to find: " + str(best_value_searchspace))
  max_value_searchspace = search_space.max()
  for l in range(len(n_rogue_agents)):
    relative_deviations_per_visit=[]
    iterations_per_visit = []
    for i, (i_agent, swarms) in enumerate(results[l]):
      # metrics
      iterations = [len(s.global_best_solution) for s in swarms]
      print("iterations: " + str(iterations))
      positions = [s.global_best_solution[-1] for s in swarms]
      print("positions: " + str(positions))
      solutions = np.array([search_space[tuple(pos)] for pos, n_votes in positions if pos != None])
      print("solutions: " + str(solutions))
      relative_deviations = (np.abs(solutions - best_value_searchspace) / max_value_searchspace) * 100
      print("relative deviations: " + str(relative_deviations))
      relative_deviations_per_visit.append(np.average(relative_deviations))
      iterations_per_visit.append(np.average(iterations))
      # plot
    if l <= len(n_visits)//2:
      ax[0].plot(n_visits, relative_deviations_per_visit, label=str(l+1)+' rogue agents')
      ax[1].plot(n_visits, iterations_per_visit)
      ax[0].legend(loc='upper center', bbox_to_anchor=(0.5, -0.2),fancybox=False, shadow=False, ncol=1)
    elif l > len(n_visits)//2:
      ax[0].plot(n_visits, relative_deviations_per_visit)
      ax[1].plot(n_visits, iterations_per_visit, label= str(l+1)+' rogue agents')
      ax[1].legend(loc='upper center', bbox_to_anchor=(0.5, -0.2),fancybox=False, shadow=False, ncol=1)

  ax[0].set_xlabel('Number of visits')
  ax[0].set_ylabel('Relative deviation to best solution [%]')
  ax[1].set_xlabel('Number of visits')
  ax[1].set_ylabel('Number of iterations')
  plt.savefig("./paper/Figures/plots/n_visits_vs_rogue_{}.pdf".format(str(round(search_space[rogue_x,rogue_y], 3))), format = 'pdf')

def optimize_by_consensus_rounds(n_agents, n_visits, n_neighbors, n_consensus,
  majority_threshold, amount_of_noise,search_space, runs):

  result = []
  # grid search for each number of agents
  for n in n_consensus:
    # set bounds for search space matrix
    bounds = (0, [high - 1 for high in search_space.shape])
    swarms_this_run = []
    for run in range(runs):
      # create swarm
      swarm = create_swarm(n_agents, n_visits, 2, n_neighbors, n, majority_threshold, bounds,
                           0, np.array([]), amount_of_noise)
      swarm.global_best_solution = []
      swarm.vote_history = []
      # optimization loop
      for i in range(1000):
        # search space
        swarm.compute_cost(search_space)
        swarm.compute_best_position()
        # share best position among swarm (with n_neighbors)
        # update position (next n_visits)
        swarm.compute_next_position()
        # vote for best position
        swarm.compute_vote()
        # democratic consensus (majority n_consensus times)
        if swarm.has_reached_consensus():
          break
      # store state of swarm for a single run
      swarms_this_run.append(swarm)

    # store swarms for all runs
    result.append((n, swarms_this_run))

  # evaluation
  fig, ax = plt.subplots(nrows=1, ncols=2, constrained_layout=True)
  fig.suptitle("Number of rounds for consensus (search space size=" + str(search_space.size) + ",\n" +
              "number of agents="+str(n_agents)+ ")")
  best_value_searchspace = search_space.min()
  print("Best value to find: " + str(best_value_searchspace))
  max_value_searchspace = search_space.max()

  for i, (i_agent, swarms) in enumerate(result):
    # metrics
    iterations = [len(s.global_best_solution) for s in swarms]
    print("iterations: " + str(iterations))
    positions = [s.global_best_solution[-1] for s in swarms]
    print("positions: " + str(positions))
    solutions = np.array([search_space[tuple(pos)] for pos, n_votes in positions if pos != None])
    print("solutions: " + str(solutions))
    relative_deviations = (np.abs(solutions - best_value_searchspace) / max_value_searchspace) * 100
    print("relative deviations: " + str(relative_deviations))

    # plot
    ax[0].boxplot(relative_deviations, positions=[i], labels=[i_agent])
    ax[1].boxplot(iterations, positions=[i], labels=[i_agent])

  ax[0].set_xlabel('Number of rounds needed for consensus')
  ax[0].set_ylabel('Relative deviation to best solution [%]')
  ax[1].set_xlabel('Number of rounds needed for consensus')
  ax[1].set_ylabel('Number of iterations')
  plt.savefig("./paper/Figures/plots/consensus_rounds_optimization.pdf", format = 'pdf')

def optimize_by_majority_needed(n_agents, n_visits, n_neighbors, n_consensus,
  majority_threshold, amount_of_noise,search_space, runs):

  result = []
  # grid search for each number of agents
  for threshold in majority_threshold:
    # set bounds for search space matrix
    bounds = (0, [high - 1 for high in search_space.shape])
    swarms_this_run = []
    for run in range(runs):
      # create swarm
      swarm = create_swarm(n_agents, n_visits, 2, n_neighbors, n_consensus, threshold, bounds,
                           0, np.array([]), amount_of_noise)
      swarm.global_best_solution = []
      swarm.vote_history = []
      # optimization loop
      for i in range(1000):
        # search space
        swarm.compute_cost(search_space)
        swarm.compute_best_position()
        # share best position among swarm (with n_neighbors)
        # update position (next n_visits)
        swarm.compute_next_position()
        # vote for best position
        swarm.compute_vote()
        # democratic consensus (majority n_consensus times)
        if swarm.has_reached_consensus():
          break
      # store state of swarm for a single run
      swarms_this_run.append(swarm)

    # store swarms for all runs
    result.append((threshold, swarms_this_run))

  # evaluation
  fig, ax = plt.subplots(nrows=1, ncols=2, constrained_layout=True)
  fig.suptitle("Threshold for majority (search space size=" + str(search_space.size)+ ", number of agents="+str(n_agents)+ ")")
  best_value_searchspace = search_space.min()
  print("Best value to find: " + str(best_value_searchspace))
  max_value_searchspace = search_space.max()

  for i, (i_agent, swarms) in enumerate(result):
    # metrics
    iterations = [len(s.global_best_solution) for s in swarms]
    print("iterations: " + str(iterations))
    positions = [s.global_best_solution[-1] for s in swarms]
    print("positions: " + str(positions))
    solutions = np.array([search_space[tuple(pos)] for pos, n_votes in positions if pos != None])
    print("solutions: " + str(solutions))
    relative_deviations = (np.abs(solutions - best_value_searchspace) / max_value_searchspace) * 100
    print("relative deviations: " + str(relative_deviations))

    # plot
    ax[0].boxplot(relative_deviations, positions=[i], labels=[i_agent])
    ax[1].boxplot(iterations, positions=[i], labels=[i_agent])

  ax[0].set_xlabel('Majority threshold')
  ax[0].set_ylabel('Relative deviation to best solution [%]')
  ax[1].set_xlabel('Majority threshold')
  ax[1].set_ylabel('Number of iterations')
  plt.savefig("./paper/Figures/plots/majority_threshold.pdf", format = 'pdf')

def optimize_by_n_neighbor(n_agents, n_visits, n_neighbors, n_consensus, majority_threshold, search_space, runs):
  result = []
  
  # grid search for each number of neighbors incl. visits
  for n_visit, n_neighbor in zip(n_visits, n_neighbors):
    # set bounds for search space matrix
    bounds = (0, [high-1 for high in search_space.shape])

    swarms_this_run = []

    for run in range(runs):
      # create swarm
      swarm = create_swarm(15, n_visit, 2, n_neighbor, n_consensus, majority_threshold, bounds,
        0, np.array([]), .0)
      
      # todo: list defined with attrs do not get set to default [], instead reference is kept?!?
      swarm.global_best_solution = []
      swarm.vote_history = []

      # optimization loop
      for i in range(1000):
        # search space
        swarm.compute_cost(search_space)
        swarm.compute_best_position()

        # share best position among swarm (with n_neighbors)
        # update position (next n_visits)
        swarm.compute_next_position()

        # vote for best position
        swarm.compute_vote()

        # democratic consensus (majority n_consensus times)
        if swarm.has_reached_consensus():
          break

      # store state of swarm for a single run
      swarms_this_run.append(swarm)
      
    # store swarms for all runs
    result.append((n_neighbor, swarms_this_run))

  # evaluation
  fig, ax = plt.subplots(nrows=1, ncols=2, constrained_layout=True)
  fig.suptitle("Number of neighbors to share best position with\n" +
              "(search space size=" + str(search_space.size) + ", " +
              "number of agents=" + str(n_agents) + ")")

  best_value_searchspace = search_space.min()
  print("Best value to find: " + str(best_value_searchspace))
  max_value_searchspace = search_space.max()

  for i, (i_neighbor, swarms) in enumerate(result):
    # metrics
    iterations = [len(s.global_best_solution) for s in swarms]
    print("iterations: " + str(iterations))
    positions = [s.global_best_solution[-1] for s in swarms]
    print("positions: " + str(positions))
    solutions = np.array([search_space[tuple(pos)] for pos, n_votes in positions if pos != None])
    print("solutions: " + str(solutions))
    relative_deviations = (np.abs(solutions - best_value_searchspace) / max_value_searchspace) * 100
    print("relative deviations: " + str(relative_deviations))

    # plot
    ax[0].boxplot(relative_deviations, positions=[i], labels=[i_neighbor])
    ax[1].boxplot(iterations, positions=[i], labels=[i_neighbor])

  ax[0].set_xlabel('Number of neighbors')
  ax[0].set_ylabel('Relative deviation to best solution [%]')
  ax[1].set_xlabel('Number of neighbors')
  ax[1].set_ylabel('Number of iterations')
  plt.savefig("./paper/Figures/plots/n_neighbor_optimization{}.pdf".format(search_space.size), format = 'pdf')

def optimize_n_visits_vs_n_neighbors(n_agents, n_visits, n_neighbors, n_consensus, majority_threshold, search_space, runs):

  results = []

  # grid search for each number of neighbors vs. number of visits
  for n_visit in n_visits:
    result = []

    for n_neighbor in n_neighbors:
      # set bounds for search space matrix
      bounds = (0, [high - 1 for high in search_space.shape])
      swarms_this_run = []
      for run in range(runs):
        # create swarm
        swarm = create_swarm(n_agents, n_neighbor + n_visit, 2, n_neighbor, n_consensus, majority_threshold, bounds,
                             0, np.array([]), .0)
        swarm.global_best_solution = []
        swarm.vote_history = []
        # optimization loop
        for i in range(1000):
          # search space
          swarm.compute_cost(search_space)
          swarm.compute_best_position()
          # share best position among swarm (with n_neighbors)
          # update position (next n_visits)
          swarm.compute_next_position()
          # vote for best position
          swarm.compute_vote()
          # democratic consensus (majority n_consensus times)
          if swarm.has_reached_consensus():
            break
        # store state of swarm for a single run
        swarms_this_run.append(swarm)

      # store swarms for all runs
      result.append((n_neighbor, swarms_this_run))
    results.append(result)

  # evaluation
  fig, ax = plt.subplots(nrows=1, ncols=2, constrained_layout=True)
  fig.suptitle("Number of neighbors vs. number of visits (search space size=" + str(search_space.size) + ",\n" +
                "number of agents="+str(n_agents) + ")")

  best_value_searchspace = search_space.min()
  print("Best value to find: " + str(best_value_searchspace))
  max_value_searchspace = search_space.max()

  for l in range(len(n_visits)):
    relative_deviations_per_visit=[]
    iterations_per_visit = []
    for i, (n_neighbor, swarms) in enumerate(results[l]):
      # metrics
      iterations = [len(s.global_best_solution) for s in swarms]
      print("iterations: " + str(iterations))
      positions = [s.global_best_solution[-1] for s in swarms]
      print("positions: " + str(positions))
      solutions = np.array([search_space[tuple(pos)] for pos, n_votes in positions if pos != None])
      print("solutions: " + str(solutions))
      relative_deviations = (np.abs(solutions - best_value_searchspace) / max_value_searchspace) * 100
      print("relative deviations: " + str(relative_deviations))
      relative_deviations_per_visit.append(np.average(relative_deviations))
      iterations_per_visit.append(np.average(iterations))
      print("neighbors: " + str([s.n_neighbors for s in swarms]))
      print("visits: " + str([s.n_visits for s in swarms]))
      # plot
    if l <= len(n_visits)//2:
      ax[0].plot(n_neighbors, relative_deviations_per_visit, label='neighbor+' + str(n_visits[l]) + ' visits')
      ax[1].plot(n_neighbors, iterations_per_visit)
      ax[0].legend(loc='upper center', bbox_to_anchor=(0.5, -0.2),fancybox=False, shadow=False, ncol=1)
    elif l > len(n_visits)//2:
      ax[0].plot(n_neighbors, relative_deviations_per_visit)
      ax[1].plot(n_neighbors, iterations_per_visit, label='neighbor+' + str(n_visits[l]) + ' visits')
      ax[1].legend(loc='upper center', bbox_to_anchor=(0.5, -0.2),fancybox=False, shadow=False, ncol=1)

  ax[0].set_xlabel('Number of neighbors')
  ax[0].set_ylabel('Relative deviation to best solution [%]')
  ax[1].set_xlabel('Number of neighbors')
  ax[1].set_ylabel('Number of iterations')
  plt.savefig("./paper/Figures/plots/n_visits_vs_n_neighbors_sspace{}.pdf".format(search_space.size), format = 'pdf')

def optimize_majority_vs_n_consensus(n_agents, n_visits, n_neighbors, n_consensus, majority_threshold, search_space, runs):

  results = []

  # grid search for each number of neighbors vs. number of visits
  for consensus in n_consensus:
    result = []

    for majority in majority_threshold:
      # set bounds for search space matrix
      bounds = (0, [high - 1 for high in search_space.shape])
      swarms_this_run = []
      for run in range(runs):
        # create swarm
        swarm = create_swarm(n_agents, n_visits, 2, n_neighbors, consensus, majority, bounds,
                             0, np.array([]), .0)
        swarm.global_best_solution = []
        swarm.vote_history = []
        # optimization loop
        for i in range(1000):
          # search space
          swarm.compute_cost(search_space)
          swarm.compute_best_position()
          # share best position among swarm (with n_neighbors)
          # update position (next n_visits)
          swarm.compute_next_position()
          # vote for best position
          swarm.compute_vote()
          # democratic consensus (majority n_consensus times)
          if swarm.has_reached_consensus():
            break
        # store state of swarm for a single run
        swarms_this_run.append(swarm)

      # store swarms for all runs
      result.append((majority, swarms_this_run))
    results.append(result)

  # evaluation
  fig, ax = plt.subplots(nrows=1, ncols=2, constrained_layout=True)
  fig.suptitle("Majority threshold vs. number of consensus rounds needed\n" + 
              "(search space size=" + str(search_space.size) + ", " +
              "number of agents="+str(n_agents) + ")")

  best_value_searchspace = search_space.min()
  print("Best value to find: " + str(best_value_searchspace))
  max_value_searchspace = search_space.max()

  for l in range(len(n_consensus)):
    relative_deviations_per_visit=[]
    iterations_per_visit = []
    for i, (m_threshold, swarms) in enumerate(results[l]):
      # metrics
      iterations = [len(s.global_best_solution) for s in swarms]
      print("iterations: " + str(iterations))
      positions = [s.global_best_solution[-1] for s in swarms]
      print("positions: " + str(positions))
      solutions = np.array([search_space[tuple(pos)] for pos, n_votes in positions if pos != None])
      print("solutions: " + str(solutions))
      relative_deviations = (np.abs(solutions - best_value_searchspace) / max_value_searchspace) * 100
      print("relative deviations: " + str(relative_deviations))
      relative_deviations_per_visit.append(np.average(relative_deviations))
      iterations_per_visit.append(np.average(iterations))
      print("majority t: " + str([s.majority_threshold for s in swarms]))
      print("n_consensus: " + str([s.n_consensus for s in swarms]))
      # plot
    if l <= len(n_consensus)//2:
      ax[0].plot(majority_threshold, relative_deviations_per_visit, label='number of consensus ' + str(n_consensus[l]))
      ax[1].plot(majority_threshold, iterations_per_visit)
      ax[0].legend(loc='upper center', bbox_to_anchor=(0.5, -0.2),fancybox=False, shadow=False, ncol=1)
    elif l > len(n_consensus)//2:
      ax[0].plot(majority_threshold, relative_deviations_per_visit)
      ax[1].plot(majority_threshold, iterations_per_visit, label='number of consensus ' + str(n_consensus[l]))
      ax[1].legend(loc='upper center', bbox_to_anchor=(0.5, -0.2),fancybox=False, shadow=False, ncol=1)


  ax[0].set_xlabel('Majority threshold')
  ax[0].set_ylabel('Relative deviation to best solution [%]')
  ax[1].set_xlabel('Majority threshold')
  ax[1].set_ylabel('Number of iterations')
  plt.savefig("./paper/Figures/plots/majthreshold_vs_n_consensus.pdf", format = 'pdf')
  plt.show()