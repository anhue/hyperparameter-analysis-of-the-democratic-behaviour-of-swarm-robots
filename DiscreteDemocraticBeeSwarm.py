from attr import attrib, attrs
from attr.validators import instance_of
import numpy as np
from collections import Counter

@attrs
class DiscreteDemocraticBeeSwarm(object):
  """Represents a discrete swarm

  Attributes
  ----------
  n_agents : int
    number of agents in the swarm.
  n_visits : int
    number of positions an agent can visit per iteration.
  n_dimensions : int
    number of dimensions of the search space.
  n_neighbors : int
    number of neighbors an agent can communicate with (advertise his personal best solution).
  n_consensus : int
    number of times in a row, a global solution has to appear, to reach consensus.
  majority_threshold : float
    threshold to define majority in range :code:`[0,1)`, that a given fraction of a swarm votes for a given
    solution and this threshold is exceeded that it is a valid majority.
  bounds : tuple
    tuple of shape :code:`(low, high)` to define the lower and upper bounds of 
    the search space in each dimension respectively.
  position : numpy.ndarray
    matrix of positions of shape :code:`(n_agents, n_visits, n_dimensions)` to store the 
    positions of all agents within a given iteration.
  n_rogue_agents : int
    number of rogue agents in the swarm. They will always promote a certain position according to rogue_agent_positions.
  rogue_agent_positions : numpy.ndarray
    matrix of positions, rogue agent(s) will promote, of shape :code:`(n_rogue_agents, n_dimensions)`.
  noise : float
    noise to add to the cost of the function in the form of :code:`np.normal() * noise`.
  cost : numpy.ndarray
    matrix of costs of shape :code:`(n_agents, n_visits)` to store the costs of all agents 
    for all visits (same index `n_visits` as in position matrix) within a given iteration.
  best_position : numpy.ndarray
    matrix of best positions of shape :code:`(n_agents, n_dimensions)` to store the best position 
    (out of `n_visits` visits) for all agents respectively within a given iteration.
  global_best_solution : list
    list of global best solutions the swarm voted on for all iterations. Each item is of shape 
    :code:`(position, number_of_votes)` where position is a tuple containing coordinate for each dimension.
    A solution is only added to this list, if majority is reached (>50% voted for same).
  vote_history: list
    list of all votes (solution & number of votes) until consensus was reached.
  """
  n_agents = attrib(type=int, validator=instance_of(int))
  n_visits = attrib(type=int, validator=instance_of(int))
  n_dimensions = attrib(type=int, validator=instance_of(int))
  n_neighbors = attrib(type=int, validator=instance_of(int))
  n_consensus = attrib(type=int, validator=instance_of(int)) 
  majority_threshold = attrib(type=float, validator=instance_of(float))
  bounds = attrib(type=tuple, validator=instance_of(tuple))
  position = attrib(type=np.ndarray, validator=instance_of(np.ndarray))
  n_rogue_agents = attrib(
    type=int,
    default=0,
    validator=instance_of(int)
  )
  rogue_agent_positions = attrib(
    type=np.ndarray,
    default=np.array([]),
    validator=instance_of(np.ndarray)
  )
  noise = attrib(
    type=float,
    default=0,
    validator=instance_of(float)
  )
  cost = attrib(
    type=np.ndarray,
    default=np.array([]),
    validator=instance_of(np.ndarray)
  )
  last_best_cost = attrib(
    type=np.ndarray,
    default=np.array([]),
    validator=instance_of(np.ndarray)
  )
  best_position = attrib(
    type=np.ndarray,
    default=np.array([]),
    validator=instance_of(np.ndarray)
  )
  global_best_solution = attrib(
    type=list,
    default=[],
    validator=instance_of(list)
  )
  vote_history = attrib(
    type=list,
    default=[],
    validator=instance_of(list)
  )
  rng = np.random.default_rng(seed=42)

  def compute_cost(self, search_space):
    """Computes the cost of the given search space and stores them in  the field `cost`.
    
    Parameters
    ----------
    searchSpace : numpy.ndarray
      matrix with `n_dimensions` dimensions to represent the search space (qualities/values)
    """
    # recall best cost from memory
    if self.cost.size > 0:
      if self.last_best_cost.size > 0:
        self.last_best_cost = np.minimum(self.cost.min(axis=1), self.last_best_cost)
      else:
        self.last_best_cost = self.cost.min(axis=1)

    # calculate new cost
    cost = [search_space[tuple(self.position[:, visit, :].T)] for visit in range(self.n_visits)]
    self.cost = np.array(cost).T

    # set cost of rogue agents to 0
    if self.n_rogue_agents:
      self.cost[range(self.n_rogue_agents), :] = [0] * self.n_visits
    
    # apply noise
    if self.noise:
      self.cost += self.rng.normal(size=(self.n_agents, self.n_visits)) * self.noise

  def compute_best_position(self):
    """Computes the best position for each agent and stores them in the field `best_position`."""
    # extend cost with last best cost = virtual memory
    if self.last_best_cost.size > 0:
      cost_with_memory = np.append(self.cost, self.last_best_cost.reshape(self.last_best_cost.shape[0], 1), axis=1)
    else:
      # there is no memory in first iteration
      cost_with_memory = self.cost

    # get min cost for each agent, if multiple positions have equal cost choose one randomly
    idc = np.array([self.rng.choice(np.where(cost == cost.min())[0]) for cost in cost_with_memory])

    # extend position with last best position = virtual memory
    if self.last_best_cost.size > 0:
      position_with_memory = np.append(self.position, self.best_position.reshape(self.best_position.shape[0], 1, self.n_dimensions), axis=1)
    else:
      # there is no memory in first iteration
      position_with_memory = self.position

    # update best position
    self.best_position = position_with_memory[range(self.n_agents), idc]

  def compute_next_position(self):
    """Computes the next positions to visit in the next iteration and stores them in the field `position`."""
    # advertise best position with n_neighbors
    # random shuffling to generate random neighborhood
    idc_neighborhood = np.arange(self.n_agents)
    self.rng.shuffle(idc_neighborhood)
    # generate neighborhood for n_neighbors
    idc_shifted = [np.concatenate((idc_neighborhood[(n + 1):self.n_agents], idc_neighborhood[0:(n + 1)])) for n in range(self.n_neighbors)]
    # set advertised positions
    for neighbor in range(self.n_neighbors):
      self.position[idc_neighborhood, neighbor, :] = self.best_position[idc_shifted[neighbor]]

    # fill remaining position(s) with random position(s)
    low, high = self.bounds
    for visit in range(self.n_neighbors, self.n_visits):
      self.position[:, visit, :] = self.rng.integers(low=low, high=high, size=(self.n_agents, self.n_dimensions), endpoint=True)

    # set positions of rogue agents static
    if self.n_rogue_agents:
      for visit in range(self.n_visits):
        self.position[range(self.n_rogue_agents), visit, :] = self.rogue_agent_positions

  def compute_vote(self):
    """Computes the votes for each agent and if majority is met, stores the vote in the field `global_best_solution`."""
    votes = Counter([tuple(x) for x in self.best_position])

    # track vote history
    self.vote_history.append(votes)

    # if only one solution is left -> all agents vote for this solution
    if len(votes.most_common()) == 1:
      self.global_best_solution.append(votes.most_common()[0])
      return

    # check for majority (a single solution with the most votes is needed)
    (best_solution, best_n_votes), (second_solution, second_n_votes) = votes.most_common(2)
    if best_n_votes > second_n_votes:
      # majority is for best_solution
      # check if vote threshold is also reached
      if (best_n_votes / self.n_agents) > self.majority_threshold:
        self.global_best_solution.append((best_solution, best_n_votes))
      else:
        # None indicates that in the given iteration was no majority vote
        self.global_best_solution.append((None, None))
    else:
      # no majority reached, best and second solution have same amount of votes
      self.global_best_solution.append((None, None))

  def has_reached_consensus(self):
    """Check if swarm has reached consensus.

    The same solution has to appear `n_consensus` times in a row in the field `global_best_solution`.

    Returns
    -------
    bool
      true, if consensus is reached, else false.
    """
    last_n_consensus = self.global_best_solution[-self.n_consensus:]
    # check if enough entries and not series of no votes
    if (len(last_n_consensus) >= self.n_consensus) & ((None, None) not in last_n_consensus):
      # check if all are same
      return len({best_solution for best_solution, n_votes in last_n_consensus}) == 1
    else:
      return False

def create_swarm(n_agents, n_visits, n_dimensions, n_neighbors, n_consensus, majority_threshold, bounds,
  n_rogue_agents, rogue_agent_positions, noise):
  """Creates a DemocraticBeeSwarm object
  
  See also
  --------
  DemocraticBeeSwarm : for a description of the parameters
  """
  rng =  np.random.default_rng(seed=42)
  low, high = bounds

  # generate initial positions
  pos = rng.integers(low=low, high=high, size=(n_agents, n_visits, n_dimensions), endpoint=True)

  # set static postions for rogue agents
  if n_rogue_agents:
    for visit in range(n_visits):
      pos[range(n_rogue_agents), visit, :] = rogue_agent_positions

  return DiscreteDemocraticBeeSwarm(
    n_agents,
    n_visits,
    n_dimensions,
    n_neighbors,
    n_consensus,
    majority_threshold,
    bounds,
    pos,
    n_rogue_agents,
    rogue_agent_positions,
    noise
  )
