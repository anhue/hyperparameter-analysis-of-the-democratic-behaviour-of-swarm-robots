# Prerequisities
- Python 3
- [numpy](https://pypi.org/project/numpy/)
- [matplotlib](https://pypi.org/project/matplotlib/)

To install a python package from the [Python Package Index](https://pypi.org/) use the following command.

```
pip install <package>
```
Refer to [use pip for installing](https://packaging.python.org/tutorials/installing-packages/#use-pip-for-installing) for further instructions.

# Run
To run our script `main.py` which produces all the figures used in the paper, run the following command.
```
python main.py
```

# Introduction
|File|Description|
|--|--|
|[main.py](main.py)|Startscript used to generate all the figures for the paper.|
|[DiscreteDemocraticBeeSwarm.py](DiscreteDemocraticBeeSwarm.py)|Implementation of the swarm according to the democratic behavior of honeybees. This swarm only works on a discrete search space meaning on a predefined numpy matrix. **All parameters of the swarm** are described in this file.|
|[utils.py](utils.py)|Offers functionalities to create a discrete search space (numpy matrix) from a continuous function (e.g. rastrigin).|
|[GridSearchOptimizer.py](GridSearchOptimizer.py)|Offers functionalities to create all different figures. For that it is creating the swarm and executing the optimization loop.|

> **NOTE:**
> When you plan to use the [DiscreteDemocraticBeeSwarm.py](DiscreteDemocraticBeeSwarm.py) in your own project, be aware that the default random generator (from numpy) is set to a seed of `42`. You may change this according to your needs. It has to be changed in two places:
> 1. In the function `create_swarm()`
> 2. In the field `rng`