\documentclass[conference]{IEEEtran}
\IEEEoverridecommandlockouts

% The preceding line is only needed to identify funding in the first footnote. If that is unneeded, please comment it out.
\usepackage[ruled,vlined]{algorithm2e}
\usepackage{cite}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{xcolor}
\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08em
    T\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}
\begin{document}

\title{Conference Paper Title*\\
{\footnotesize \textsuperscript{*}Note: Sub-titles are not captured in Xplore and
should not be used}
}

\author{\IEEEauthorblockN{Andreas Hügli}
\IEEEauthorblockA{\textit{Medical Informatics} \\
\textit{University of Applied Sciences, FHNW Muttenz}\\
Basel-Land, Switzerland\\
andreas.huegli@students.fhnw.ch}
\and
\IEEEauthorblockN{Marco A. G. Pereira}
\IEEEauthorblockA{\textit{Medical Informatics} \\
\textit{University of Applied Sciences, FHNW Muttenz}\\
Basel-Land, Switzerland\\
marco.guimaraees@students.fhnw.ch}
}

\maketitle

\begin{abstract}
Swarm robots can be used to solve exploration problems, such as the best-of-n problem. We implement a baseline and investigate the efficiency and robustness of the consensus achievement in the democratic bees algorithm. We show that the algorithm is very viable ... All results are based on computational experiments. 
\end{abstract}

\begin{IEEEkeywords}
best-of-n, swarm robotics, democracy, consensus achievement, collective decision making
\end{IEEEkeywords}

\section{Introduction}
When a collective of agents makes a decision that is not traceable to any single individual agent, we speak of collective decision making. This has been observed in multiple swarms of social insects in the nature. Collective decision making can be divided in two categories: consensus achievement and task allocation. We investigate the discrete consensus achievement. Swarm robotics aims at developing systems that are scalable and robust to noise along with malfunctioning individual agents. For this purpose, swarms are designed to have no leader and to explore areas locally without access to global information \cite{10.3389/frobt.2017.00009}. In \cite{pochon_investigating_2018} the authors use a nature-inspired model that uses the democratic behavior of honeybees, when individually exploring new locations for a hive, but deciding for the best location as a collective, to solve the best-of-n problem. As it is a very unique approach in the literature, we hereby set a baseline for future research of the algorithm by implementing the approach and tuning its hyperparameters.

\section{Democratic bees optimization}
In \cite{pochon_investigating_2018} the best-of-n problem is approached using a finite state machine with four states. Individual agents explore distinct parts of the whole area, visiting three random locations and storing the best scores in an exploration table (Exploration state). Afterwards, the agents communicate their best opinion with their neighbours, implicitly with hints, to where it can be found (dissemination state). In the broadcast state, the agents vote for their best location found. Consensus is only achieved when the majority votes for the same solution three consecutive times, where the exploration can stop (final state). Deriving from this description, we notice there are plenty of hyperparameters that can be tuned, for efficient results or even a robust algorithm.

\section*{Experiments}

A total of 10 runs with n {5, 10, 15, 30, 100} agents were conducted in a search space {25,100,400,900,10000,90000} with discretised values of the well known Rastrigin function. To provide a starting point, the first experiments were performed using perfect conditions, so that no noise was injected and no use of rogue agents implemented, see algorithm \ref{pseudocode}. To start initial experiments, the influence of the number of agents and search space size was examined (see image \ref{agents_optm}) using boxplots of 10 runs in total. Towards that end, we decided to use the documented parameters in the original paper. Parameters for our algorithm with the original values in parenthesis, are: number of agents (15), number of rogue agents = (0), number of visits per individual (3), number of rounds necessary to achieve consensus (3), noise (0), percentage to count as majority (0.5) and search space (255).With the setup found, further experiments were conducted to evaluate the effect of rogue agents or noisy values in the algorithm, as to determine its robustness. When implementing rogue agents, they were given a location corresponding to a solution that they were supposed to advertise, not considering any other locations. For the purpose of bias reduction, four different experiments were performed, using four different locations with small to large deviations form true value. When considering noise, a value was sampled from a normal distribution, multiplied by a factor, and introduced to the value found by an agent, in the range: (0.0,0.5,1.0,5.0,10.0,20.0,30.0). Based on these reports, and on account of the high amount of parameters in the algorithm, further investigations took place to find trends on the potential enhancement of the robustness against these factors. For this purpose, we performed experiments on how the number of visits, performed by an individual agent, affects the influence of rogue agents in the swarm. 

\begin{algorithm}
\SetAlgoLined
\KwResult{Position and value of the solution}
 Initialise population with n random locations per individual\;
 \For{i iterations}{
  Evaluate value at the position of each individual\;
  Evaluate best position of the n visits for each individual\;
  Advertise positions to visit in the next iteration\;
  Vote for best solution\;
  \uIf{consensus was achieved n times consecutively on the best solution}{
   Solution found\;}
 }
 \label{pseudocode}
 \caption{Democratic bees optimization method}
\end{algorithm}

\section*{Results}

We started by evaluating differences in relative deviation to the best solution in the search space and time to consensus when increasing the search space (see figure \ref{searchspace_optm}). Herewith, we observe that with a search space of 900 elements, we keep the accuracy of the swarm, while the number of iterations to reach a solution stays consistent. 

\begin{figure}[htbp]
\includegraphics[width=0.5\textwidth]{Figures/plots/searchspace_optimization.png}
\caption{Search space size plotted against deviation of correct solution and number of iterations needed}
\label{searchspace_optm}
\end{figure}

When plotting the affect of the number of agents in the swarm against the predefined search space, we come to the number 15 as an initial value, analogous to the original paper. In figure \ref{agents_optm} we can see that 15 agents provide us with accurate results, while fairly fast. 

\begin{figure}[htbp]
\includegraphics[width=0.5\textwidth]{Figures/plots/number_of_agents_optimization.png}
\caption{Number of agents plotted against deviation of correct solution and number of iterations needed}
\label{agents_optm}
\end{figure}

To test the robustness of the algorithm, the number of agents was increased from 0 to 10. Herewith we observe that the algorithm behaves robustly against a maximum of six agents, with seven agents, however, the algorithm collapses and is not able to find the right solution anymore. We were expecting it to break at eight rogue agents as it is more than half of the swarm (15 agents) and therefore the majority. It is also noticeable, that even with a robust behavior, the swarm has more iterations to complete the task, unless with rogue agents > 8 where the algorithm converges very fast to the wrong solution. 

\begin{figure}[htbp]
\includegraphics[width=0.5\textwidth]{Figures/plots/rogue_agents_bad_quality.png}
\caption{Number of rogue agents advertising the score 58 against deviation of correct solution and number of iterations needed}
\label{r_agents_optm}
\end{figure}

Improving the quality score by a large amount towards 33.352, we observe a similar behavior, however the model starts to collapse with > 4 number of rogue agents, completely losing the ability to solve the problem with > 6 rogue agents also very well interpretable in the graph to the right as we see time to convergence increasing from zero to five number of agents and then decreasing from six to nine, as rogue agents completely take over the swarm. 

\begin{figure}[htbp]
\includegraphics[width=0.5\textwidth]{Figures/plots/rogue_agents_medium_quality.png}
\caption{Number of rogue agents advertising the score 33.352 against deviation of correct solution and number of iterations needed}
\label{agents_optm}
\end{figure}

By increasing the quality of the advertised location of the rogue agents, we observe the same trend as before, shifted to the left. Therefore now, five rogue agents are already enough, to mislead the swarm. 

\begin{figure}[htbp]
\includegraphics[width=0.5\textwidth]{Figures/plots/rogue_agents_medium_quality2.png}
\caption{Number of agents plotted against deviation of correct solution and number of iterations needed}
\label{agents_optm}
\end{figure}

Finally, experimenting with rogue agents advertising a quality score very close to the correct quality score, a number > two rogue agents is enough to confuse the whole swarm. 

\begin{figure}[htbp]
\includegraphics[width=0.5\textwidth]{Figures/plots/rogue_agents_good_quality.png}
\caption{Number of agents plotted against deviation of correct solution and number of iterations needed}
\label{agents_optm}
\end{figure}

\subsection{Figures and Tables}
\paragraph{Positioning Figures and Tables} Place figures and tables at the top and 
bottom of columns. Avoid placing them in the middle of columns. Large 
figures and tables may span across both columns. Figure captions should be 
below the figures; table heads should appear above the tables. Insert 
figures and tables after they are cited in the text. Use the abbreviation 
``Fig.~\ref{fig}'', even at the beginning of a sentence.

\begin{table}[htbp]
\caption{Table Type Styles}
\begin{center}
\begin{tabular}{|c|c|c|c|}
\hline
\textbf{Table}&\multicolumn{3}{|c|}{\textbf{Table Column Head}} \\
\cline{2-4} 
\textbf{Head} & \textbf{\textit{Table column subhead}}& \textbf{\textit{Subhead}}& \textbf{\textit{Subhead}} \\
\hline
copy& More table copy$^{\mathrm{a}}$& &  \\
\hline
\multicolumn{4}{l}{$^{\mathrm{a}}$Sample of a Table footnote.}
\end{tabular}
\label{tab1}
\end{center}
\end{table}

Figure Labels: Use 8 point Times New Roman for Figure labels. Use words 
rather than symbols or abbreviations when writing Figure axis labels to 
avoid confusing the reader. As an example, write the quantity 
``Magnetization'', or ``Magnetization, M'', not just ``M''. If including 
units in the label, present them within parentheses. Do not label axes only 
with units. In the example, write ``Magnetization (A/m)'' or ``Magnetization 
\{A[m(1)]\}'', not just ``A/m''. Do not label axes with a ratio of 
quantities and units. For example, write ``Temperature (K)'', not 
``Temperature/K''.
\section*{Conclusion}

\section*{Acknowledgment}

The preferred spelling of the word ``acknowledgment'' in America is without 
an ``e'' after the ``g''. Avoid the stilted expression ``one of us (R. B. 
G.) thanks $\ldots$''. Instead, try ``R. B. G. thanks$\ldots$''. Put sponsor 
acknowledgments in the unnumbered footnote on the first page.

\section*{References}

Please number citations consecutively within brackets \cite{b1}. The 
sentence punctuation follows the bracket \cite{b2}. Refer simply to the reference 
number, as in \cite{b3}---do not use ``Ref. \cite{b3}'' or ``reference \cite{b3}'' except at 
the beginning of a sentence: ``Reference \cite{b3} was the first $\ldots$''

Number footnotes separately in superscripts. Place the actual footnote at 
the bottom of the column in which it was cited. Do not put footnotes in the 
abstract or reference list. Use letters for table footnotes.

Unless there are six authors or more give all authors' names; do not use 
``et al.''. Papers that have not been published, even if they have been 
submitted for publication, should be cited as ``unpublished'' \cite{b4}. Papers 
that have been accepted for publication should be cited as ``in press'' \cite{b5}. 
Capitalize only the first word in a paper title, except for proper nouns and 
element symbols.

For papers published in translation journals, please give the English 
citation first, followed by the original foreign-language citation \cite{b6}.

\bibliographystyle{IEEEtran}
\bibliography{IEEEexample.bib}


\color{red}
IEEE conference templates contain guidance text for composing and formatting conference papers. Please ensure that all template text is removed from your conference paper prior to submission to the conference. Failure to remove the template text from your paper may result in your paper not being published.

\end{document}
