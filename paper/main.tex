\documentclass[conference]{IEEEtran}
\IEEEoverridecommandlockouts

% The preceding line is only needed to identify funding in the first footnote. If that is unneeded, please comment it out.
\usepackage[ruled,vlined]{algorithm2e}
\usepackage{cite}
\usepackage{svg}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{xcolor}
\PassOptionsToPackage{hyphens}{url}
\usepackage[colorlinks, urlcolor=blue, linkcolor=black, citecolor=black]{hyperref}

\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08em
    T\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}
\begin{document}

\title{Hyperparameter Analysis of the Democratic Behavior of Swarm Robots\\
}

\author{\IEEEauthorblockN{Andreas Hügli}
\IEEEauthorblockA{\textit{Medical Informatics} \\
\textit{University of Applied Sciences, FHNW Muttenz}\\
Basel-Land, Switzerland\\
andreas.huegli@students.fhnw.ch}
\and
\IEEEauthorblockN{Marco A. G. Pereira}
\IEEEauthorblockA{\textit{Medical Informatics} \\
\textit{University of Applied Sciences, FHNW Muttenz}\\
Basel-Land, Switzerland\\
marco.guimaraees@students.fhnw.ch}
}

\maketitle

\begin{abstract}
Swarm robots can be used to solve exploration problems, such as the best-of-n problem. A crucial role plays the consensus achievement as the swarm needs to collectively agree on a solution. This task can be even more challenging given noise and malfunctioning or even rogue agents. We implement a baseline based on the democratic honeybees algorithm and investigate the performance and robustness of the consensus achievement. We show that the algorithm is viable and offers robustness against noise and rogue agents, to an extent, which increases when adding a memory to agents. Furthermore hyperparameter tuning was performed and also proved to help the swarm explore very large search spaces. All results are based on computational experiments. 
\end{abstract}

\begin{IEEEkeywords}
best-of-n, swarm robotics, democratic honeybees, consensus achievement, collective decision making
\end{IEEEkeywords}

\section{Introduction}
When a collective of agents, called a swarm, makes a decision that is not traceable to any single agent, we speak of collective decision making. Each agent of this swarm only has partial knowledge available in regards to the global solution the swarm has to consent to. Therefore the agents need to share their partial knowledge with the other agents and as a collective agree on a particular solution. This principle is called self-organization.

This has been observed in multiple swarms of social insects in the nature \cite{review_of_nature-inspired_algo}\cite{nicolis_self-organization_2008}. Collective decision making can be divided in two categories: consensus achievement and task allocation. We investigate the consensus achievement. Swarm robotics aims at developing systems that are scalable and robust to noise along with malfunctioning or rogue agents. For this purpose, swarms are designed to have no central authority, we speak of a decentralized swarm. A specific task for swarm robots is to solve the best-of-n problem, especially if $n>2$ \cite{10.3389/frobt.2017.00009}. Here the agents collect $n$ different options and, as a swarm, need to consent on a particular option.

In \cite{pochon_investigating_2018} the authors use a nature-inspired model that uses the democratic behavior of honeybees, inspired by Seeley's study on Honeybee Democracy\cite{seeley_honeybee_2010}. Where honeybees individually explore new positions for a hive, but deciding for the best position as a collective, to solve the best-of-n problem. As it is a very unique approach in the literature, we hereby set a baseline for future research of the algorithm, by implementing the approach and tuning its hyperparameters.

Because of the nature of collective decision making, the swarm might be highly susceptible to malicious or rogue agents. Such rogue agents can influence the performance of the swarm in various ways. Either slow down the consensus achievement or make it impossible, given a limited time frame. Moreover, such rogue agents can lead the swarm to make the wrong decision and potentially fail its task, which cannot occur in a real world mission, should this algorithm be used\cite{strobel_managing_2018}.  Recent research, addressing those problems, include use of the emerging technology blockchain, to track rogue agents and eventually exclude them from the swarm\cite{strobel_blockchain_nodate}\cite{strobel_managing_2018}.

Also the effects of noise, could be disastrous, providing an agent a false sense of an option, when it might have been altered by environment changes or sensor malfunctions or inaccuracies\cite{pochon_investigating_2018}.

\section{Democratic honeybees algorithm}
In \cite{pochon_investigating_2018} the best-of-n problem is approached using a finite state machine with four states. The agents explore distinct parts of the whole search space, visiting three random positions and storing the best scores in an exploration table (exploration state). Afterwards, the agents communicate their best opinion with their neighbors, implicitly with hints, to where it can be found (dissemination state). In the broadcast state, the agents vote for their best option found. Consensus is only achieved when the majority votes for the same option three consecutive times, where the exploration can stop (final state). In algorithm 1 we provide the pseudo code for our implementation of the democratic honeybee behavior. The full code for our implementation in Python can be found in our public GitLab repository\footnote{\url{https://gitlab.com/anhue/hyperparameter-analysis-of-the-democratic-behaviour-of-swarm-robots}}.

\begin{algorithm}
  \SetAlgoLined
  \KwResult{Position and value of the solution}
   Initialize population with $n\_visits$ random locations per agent\;
   \For{i iterations}{
    Evaluate quality scores for the positions of each agent\;
    Evaluate the best position for each agent, taking last best position into consideration\;
    Advertise the best position to other agents to visit in next iteration\;
    Vote for best position\;
    \uIf{consensus was achieved $n\_consensus$ times consecutively on the best position}{
     Solution found\;}
   }
   \label{pseudocode}
   \caption{Democratic honeybee behavior}
\end{algorithm}

Deriving from this description, we notice there are plenty of hyperparameters that can be tuned, for efficient results or even a robust algorithm (see table \ref{tbl_hyperparameters_description}).

\begin{table}[htbp]
  \caption{Hyperparameters of democratic honeybee swarm}
  \begin{center}
    \begin{tabular}{|l|p{5.5cm}|}
    \hline
    \textbf{Hyperparameter} & \textbf{Description} \\
    \hline
    $n\_agents$ & The total number of agents in the swarm. \\
    \hline
    $n\_iterations$ & The total number of iterations a swarm can run its optimization loop. If this number is reached, the swarm stops its optimization without achieving consensus. \\
    \hline
    $n\_visits$ & The amount of positions an agent can visit per iteration. \\
    \hline
    $n\_neighbors$ & The number of neighbors an agent shares its best option with. Since an agent will visit this position in the next iteration, the parameter $n\_visits$ should be greater or at least equal to $n\_neighbors$. \\
    \hline
    $majority\_threshold$ & Defining a threshold of what is considered to be a majority based on a fraction of agents in the swarm. This parameter must be inside the interval $(0, 1)$. \\
    \hline
    $n\_consensus$ & Defining a threshold of how many iterations in a row the swarm has to consent to the same option, to achieve consensus. To consent to an option in a given iteration, the majority has to vote for this option. \\
    \hline
    \end{tabular}
  \label{tbl_hyperparameters_description}
  \end{center}
\end{table}

\section{Experiments}

To measure the performance of the swarm we use two endpoints. Firstly the best solution according to the swarm is compared against the true best solution of the search space, resulting in a relative deviation [in \%] to the true best solution. Obviously, if the relative deviation is $0\%$ the swarm has found the true best solution. On the other hand, we measure the number of iterations needed to achieve consensus, as speed is also an important factor. To reduce bias, we let a swarm always run 10 times with the same configuration.

As a search space, we set up the well known Rastrigin function in a discrete manner. Rastrigin is a highly multi-modal function with a symmetrical structure and bounds set to $(-5.12, 5.12)$, having in dimension $\mathbb{R}^2$ roughly $100$ local minima with a global minima at position $(0, 0)$ with a value of $0$ \cite{rastrigin_function}. In our case we transform this continuous function to a discrete function in the form of a matrix, which we call search space. The search space is populated in different sizes with steps $m$ and $n$ (corresponding to $x$ and $y$ in the continuous function), resulting in different sampling rates and therefore resolution of the original continuous function. As we have to be sure that the global minima is also sampled in our discrete matrix with a value of $0$, we have to use an uneven sampling rate for $m$ and $n$. Therefore, we will always find the global minima in the centre of the matrix.

To provide a starting point, the first experiments were performed using perfect conditions, so that no noise was injected and no use of rogue agents implemented, see algorithm \ref{pseudocode}. All experiments were done and plotted against the relative deviation from the global minima and against the number of iterations needed to achieve consensus. To start initial experiments, the influence of the number of agents $n\_agents \in \{5, 10, 15, 30, 100\}$ and search space size $\in \{25, 121, 255, 441, 961, 10'201, 90'601\}$ was examined (see figure \ref{agents_optm}).

Towards that end, we try to stick to the parameters used in \cite{pochon_investigating_2018}, but adaptations were necessary. Parameters for our algorithm with the original values from \cite{pochon_investigating_2018} in parenthesis, are: number of agents (15), number of rogue agents (0), number of visits per individual (3), number of rounds necessary to achieve consensus (3), noise (0), majority threshold (0.5) and search space (255).

With the setup found, further experiments were conducted to evaluate the affect of rogue agents or noisy values in the algorithm, as to determine its robustness. When implementing rogue agents, they were given a position corresponding to a solution that they were supposed to advertise, not considering any other positions. For the purpose of bias reduction, four different experiments were performed, using four different positions with small to large deviations form the true best solution. 

Because with more visits, agents can explore more of the search space, we test how we can take advantage of this parameter to achieve increased robustness in the swarm. The number of rogue agents used was $\in \{1,2,3,4,5,6,7\}$ and the number of visits $n\_visits \in \{2,3,4,5,6,7,8\}$

As number of neighbors and visits dictate how the search space is explored, we suspect their tuning can improve the algorithms performance in perfect but also in manipulated conditions. Therefore we test the influence of the number of neighbors $n\_neighbors \in \{1, 2, 3, 4, 5, 6, 7, 8, 9, 10\}$ and the number of additional visits $n\_visits \in \{1, 2, 3, 4, 5, 6, 7, 8\}$ respectively.

Subsequently, we experimented with the value of the number of consensus $n\_consensus \in \{1,2,3,4,5,6,7,8\}$, as it should have an impact in the consensus achievement task of the swarm.

Evaluating the effect of the majority threshold to the consensus achievement, values $majority\_threshold \in \{0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9\}$ were tested.

To finalize comparison of multiple parameters, we compare the two parameters that dictate how a swarm achieves consensus. In that case, the number of rounds to achieve consensus $n\_consensus \in \{1,2,3,4,5,6,7,8\}$ and the majority threshold  $majority\_threshold \in\{0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9\}$ were observed.

When considering noise, a value was sampled from a normal distribution, multiplied by a factor and introduced to the quality score found by an agent. The absolute values of noise are $\in \{0.0, 0.5, 1.0, 5.0, 10.0, 20.0, 30.0\}$.

\section{Results}

We started by evaluating differences in relative deviation to the best solution in the search space and number of iterations needed to achieve consensus, when increasing the search space (see figure \ref{searchspace_optm}). Herewith we observe, that with an increasing search space, the accuracy of the swarm tends to get worse. Although, the number of iterations needed to achieve consensus seems to be constant with medians between five to seven. For the subsequent experiments we chose the search space with size of $961$ to use as a baseline, as it has a similar performance like search spaces with size $[255, 441]$, but is the largest of them.

\begin{figure}[htbp]
\includegraphics[width=0.5\textwidth]{Figures/plots/searchspace_optimization.pdf}
\caption{Search space size plotted against relative deviation from true best solution and number of iterations needed}
\label{searchspace_optm}
\end{figure}

When plotting the affect of the number of agents (see figure \ref{agents_optm}) in the swarm against the predefined search space, we see, that the more agents a swarm has, the better the solution gets. As the original paper \cite{pochon_investigating_2018} defined a swarm with $15$ agents and these show promising results, although need the most iterations to achieve consensus, we use that as our baseline.

\begin{figure}[htbp]
\includegraphics[width=0.5\textwidth]{Figures/plots/number_of_agents_optimization.pdf}
\caption{Number of agents plotted against deviation from true best solution and number of iterations needed}
\label{agents_optm}
\end{figure}

To test the robustness of the algorithm, the number of rogue agents was increased from zero to ten, with a quality score of $57.849$, corresponding to the relative deviation of $74.3 \%$ to the best solution (see figure \ref{r_agents_58}). Herewith we observe that the algorithm behaves robustly against a maximum of seven rogue agents, completely collapses with eight rogue agents. This is an expected behavior, as rogue agents $>=8$ lead directly to a majority (swarm with $15$ agents). However, the number of iterations needed to achieve consensus is slightly increasing with more rogue agents, unless with rogue agents $>= 8$, where the algorithm converges very fast to the wrong solution. It is also worth mentioning that most of the times the algorithm does not consent to one solution.

\begin{figure}[htbp]
\includegraphics[width=0.5\textwidth]{{Figures/plots/rogue_agents_57.849}.pdf}
\caption{Number of rogue agents advertising the quality score $57.849$ against deviation from true best solution and number of iterations needed}
\label{r_agents_58}
\end{figure}

Improving the quality score for the rogue agents towards $33.114$, which corresponds to a relative deviation of $42.5 \%$ to the true best solution, we observe a similar behavior (see figure \ref{r_agents_33}). Here the swarm also completely collapses with rogue agents $>= 8$. Also very well interpretable in the graph to the right, as we see the time to achieve consensus increasing from zero to seven rogue agents and for eight and nine is static, as rogue agents completely take over the swarm and after 3 iterations achieve consensus.

\begin{figure}[htbp]
\includegraphics[width=0.5\textwidth]{{Figures/plots/rogue_agents_33.114}.pdf}
\caption{Number of rogue agents advertising the quality score $33.114$ against deviation from true best solution and number of iterations needed}
\label{r_agents_33}
\end{figure}

By increasing the quality score for the rogue agents to $18.493$, with a corresponding relative deviation of $23.8 \%$, we observe an increased influence of the rogue agents (see figure \ref{r_agents_18}). Here the swarm starts to struggle with seven rogue agents, as in two runs the swarm collapses and is mislead by the rogue agents. With rogue agents $>= 8$ the swarm completely collapses. We also see, that the overall performance of the swarm has decreased compared with the two previous one.

\begin{figure}[htbp]
\includegraphics[width=0.5\textwidth]{{Figures/plots/rogue_agents_18.493}.pdf}
\caption{Number of rogue agents advertising the quality score $18.493$ against deviation from true best solution and number of iterations needed}
\label{r_agents_18}
\end{figure}

Finally, experimenting with rogue agents advertising a quality score of $9.291$, corresponding to the relative deviation of $11.9 \%$, very close to the correct quality score $0$ (see figure \ref{r_agents_9}). Here only one rogue agents is already able to mislead the swarm. However the swarm is still able, in most of the runs, to protect itself against one rogue agent. With rogue agents $[2, 3, 4]$ the swarm gets most of the time misled, but manages in several runs, to achieve consensus with a better solution, than advertised by the rogue agents. With rogue agents $>= 5$ the swarm completely collapses. This is understandable, as when analyzing the search space, we see that only $25$ fields are equally good or better than $9.291$, which makes $2 \%$ of the entire search space.

\begin{figure}[htbp]
\includegraphics[width=0.5\textwidth]{{Figures/plots/rogue_agents_9.291}.pdf}
\caption{Number of rogue agents advertising the score $9.291$ against deviation from true best solution and number of iterations needed}
\label{r_agents_9}
\end{figure}

Next we evaluate other parameters of the algorithm as the number of visits. We observe a large error when the number of visits is only two, as each agent visits only positions that have been shared with him. Therefore only in the first iteration new positions are observed and only shared afterwards. With an increased number of visits the accuracy increases, but stays constant from then on, slightly decreasing variance. As for the number of iterations (excluding the number of visits $2$, as without new positions it consents rather fast), we notice  a downwards trend, indicating that an increase of number of visits might accelerate the consensus achievement of the swarm (see figure \ref{n_visits_optm}). 

\begin{figure}[htbp]
\includegraphics[width=0.5\textwidth]{{Figures/plots/n_visits_optimization}.pdf}
\caption{Number of visits plotted against deviation from true best solution and number of iterations needed}
\label{n_visits_optm}
\end{figure}

With prior knowledge on how the rogue agents affect the swarm, parameters related to how the consensus is achieved, were tuned. In figure \ref{visits_rogue_score33}, we can clearly see, that while the number of visits performed by agents increases, the deviation from the best solution decreases, illustrating a power law relationship between the two variables. We also see, that more rogue agents lead to more iterations needed to achieve consensus, while increasing the number of visits does not have a meaningful impact, however a slight tendency upwards can be inferred. 

\begin{figure}[htbp]
\includegraphics[width=0.5\textwidth]{{Figures/plots/n_visits_vs_rogue_33.114}.pdf}
\caption{Number of visits with different number of rogue agents, advertising the quality score $33.114$, plotted against deviation of true best solution and number of iterations needed}
\label{visits_rogue_score33}
\end{figure}

Increasing the quality score advertised by rogue agents, we observe a similar trend as before, albeit the impact in the iterations becomes more clear. Increasing the number of positions visited by agents, again, appears to decrease the influence of the rogue agents in the consented solution as illustrated in figure \ref{visits_rogue_score18}. With a better quality score, we can now clearly see that increasing the number of visits, does increase the iterations needed to consent.

\begin{figure}[htbp]
\includegraphics[width=0.5\textwidth]{{Figures/plots/n_visits_vs_rogue_18.493}.pdf}
\caption{Number of visits with different number of rogue agents, advertising the quality score $18.493$, plotted against deviation of true best solution and number of iterations needed}
\label{visits_rogue_score18}
\end{figure}

The same tendency also applies for the situation with a quality score very close to the true best solution, although we lose the power law quality which becomes something more linear. Also very noticeable here, when having seven rogue agents, the swarm does not converge to a better solution, unless by increasing the number of visits to a number $>4$, as illustrated in figure \ref{visits_rogue_score8}. In the context of iterations till convergence, we see an even clearer upwards trend, which indicates an increase of iterations when increasing the number of visits.

\begin{figure}[htbp]
\includegraphics[width=0.5\textwidth]{{Figures/plots/n_visits_vs_rogue_9.291}.pdf}
\caption{Number of visits with different number of rogue agents, advertising the quality score $9.291$, plotted against deviation of true best solution and number of iterations needed}
\label{visits_rogue_score8}
\end{figure}

Next the influence of the number of neighbors is explored (see figure \ref{n_neighbor_optimization_961}), with a search space of $961$. We see, that with a neighborhood larger than three our algorithm doesn't improve the accuracy anymore, while a more narrow neighborhood decreases its accuracy. However with an increase of the number of neighbors, a decrease in the iterations needed to achieve consensus is highlighted.

In figure \ref{n_neighbor_optimization_90601} we increase the difficulty of the agents to find the true best solution, by increasing the size of the search space by a factor of approximately $94$. Increasing the number of neighbors at first seems to increase the relative deviation to the true best solution, but then stagnates with number of neighbors $>3$. Although with number of neighbors $<=3$ the accuracy has a great variance and gets abruptly very small when $>3$. In terms of speed to achieve consensus, we again observe an increase in the speed of convergence with a power law relationship quality.

\begin{figure}[htbp]
\includegraphics[width=0.5\textwidth]{{Figures/plots/n_neighbor_optimization961}.pdf}
\caption{Number of neighbors with search space size of $961$  plotted against deviation from true best solution and number of iterations needed}
\label{n_neighbor_optimization_961}
\end{figure}

\begin{figure}[htbp]
\includegraphics[width=0.5\textwidth]{{Figures/plots/n_neighbor_optimization90601}.pdf}
\caption{Number of neighbors with search space size of $90'601$  plotted against deviation from true best solution and number of iterations needed}
\label{n_neighbor_optimization_90601}
\end{figure}

As an increase of speed to convergence was highlighted, when plotting different neighborhoods, it was hypothesized, that different combinations of neighborhoods and number of visits performed could be beneficial for the performance of the swarm. In this particular case, we have to consider that the number of visits is dependent of the neighborhood size, as the advertised positions should be visited by each agent they were advertised to. Therefore, for the number of visits, we increment the size of neighborhood by a specific amount, so that agents have advertised positions but also some new random positions to visit, e.g. in figure \ref{n_visits_vs_n_neighbors_961}, the blue curve illustrates the example of having a neighborhood of size two, and $2+1$ visits.

In said figure, we observe a peak when using two and four as neighborhood size, with $+2$ and $+1$ visits respectively, but completely disappearing with a size of $>4$. It is also notable that the accuracy stays constant until a given number of neighbors is reached, where it improves in all cases to the true best solution with number of visits $> neighbor+3 visits$. Increasing the number of visits shows an eventual convergence of the swarm towards the true best solution. In terms of convergence speed, we notice an improvement with an increasing number of visits, which is plausible, since with more visits, each agent is able to explore more of the search space. Note that a faster convergence towards a solution, does not mean a faster computational method, since the visit of a position also takes time. In a real world scenario it depends on which parameter is more time sensitive.

To make significant statements, we increase the search space, first by a factor of approximately $11$ (see figure \ref{n_visits_vs_n_neighbors_10201}) and then $94$ (see figure \ref{n_visits_vs_n_neighbors_90601}). We notice similar plots as the previous one, accuracy slightly improves along with a higher number of neighbors and visits, with a sudden drop for number of visits $> neighbor+3 visits$ on a given number of neighbors. In terms of iterations, in a search space of $961$, we see the combinations neighbor $[+1,+2,+3]$ stop decreasing when lost in a local minima, however with larger search spaces, we notice them following the trend, decreasing to a number of four iterations in total.

\begin{figure}[htbp]
\includegraphics[width=0.5\textwidth]{{Figures/plots/n_visits_vs_n_neighbors_sspace961}.pdf}
\caption{Number of neighbors with different number of visits with search space $961$ plotted against deviation of true best solution and number of iterations needed}
\label{n_visits_vs_n_neighbors_961}
\end{figure}

\begin{figure}[htbp]
\includegraphics[width=0.5\textwidth]{{Figures/plots/n_visits_vs_n_neighbors_sspace10201}.pdf}
\caption{Number of neighbors with different number of visits with search space $10'201$ plotted against deviation of true best solution and number of iterations needed}
\label{n_visits_vs_n_neighbors_10201}
\end{figure}

\begin{figure}[htbp]
\includegraphics[width=0.5\textwidth]{{Figures/plots/n_visits_vs_n_neighbors_sspace90601}.pdf}
\caption{Number of neighbors with different number of visits with search space $90'601$ plotted against deviation of true best solution and number of iterations needed}
\label{n_visits_vs_n_neighbors_90601}
\end{figure}

When increasing the threshold for majority of the swarm to achieve consensus, we also observe an indifference of the algorithm towards this parameter (see figure \ref{majority_threshold_optm}). Although the accuracy seems to be better on a small $< 0.3$ threshold and on greater $>= 0.6$ thresholds, the variance on all thresholds is quite high, reaching from the true best solution to a relative deviation of almost $20 \%$. As opposed to the deviation to the true best solution, we see, that the number of iterations slightly increases, as the threshold increases, as we need a larger part of the swarm to vote for the same solution.

\begin{figure}[htbp]
\includegraphics[width=0.5\textwidth]{{Figures/plots/majority_threshold}.pdf}
\caption{Threshold for majority plotted against deviation from true best solution and number of iterations needed}
\label{majority_threshold_optm}
\end{figure}

Analyzing how the number of consensus affects the performance of the swarm, we can clearly see a trend in both of the subplots (see figure \ref{consensus_optm}). The performance increases in significant amounts whilst increasing the number of consensus rounds. The same is true for the number of iterations needed, where it is visually clear that, although the performance improves, the number of iterations needed to achieve consensus, increases as well, which is the definition of the plotted parameter.

\begin{figure}[htbp]
\includegraphics[width=0.5\textwidth]{{Figures/plots/consensus_rounds_optimization}.pdf}
\caption{Number of consensus plotted against deviation from true best solution and number of iterations needed}
\label{consensus_optm}
\end{figure}

Plotting majority threshold versus the number of rounds to consensus, we notice, that when increasing the majority threshold, that there is a downwards trend in each of the possible combinations (see figure \ref{majority_threshold_vs_consensus}). Also we notice, that the larger the number of consensus rounds, the better performance achieved by the swarm. The number of consensus rounds $[6,7,8]$ deliver similar performances, indicating that going higher might not be valuable. In the plot, with the number of iterations, we see, that each set up behaves in a similar manner, increasing the number of iterations independently of the number of consensus.

\begin{figure}[htbp]
\includegraphics[width=0.5\textwidth]{{Figures/plots/majthreshold_vs_n_consensus}.pdf}
\caption{Majority threshold with number of consensus plotted against deviation from true best solution and number of iterations needed}
\label{majority_threshold_vs_consensus}
\end{figure}

The most compelling finding when exploring the parameters of the algorithm was, that it is remarkably robust against noise. In figure \ref{noise_exploration}, it appears, as if the algorithm does not succumb to noisy solutions, with a consistent median, with a slight trend upwards for noise $> 10$, albeit the speed does seem to decrease at a faster pace. The numbers here shown, are absolute values of the added noise to the quality score found by the agents. 

\begin{figure}[htbp]
\includegraphics[width=0.5\textwidth]{{Figures/plots/noisy_exploration}.pdf}
\caption{Noise plotted against deviation from true best solution and number of iterations needed}
\label{noise_exploration}
\end{figure}

\section{Conclusion}

Implementing the model described in \cite{pochon_investigating_2018}, we tuned the hyperparameters to test its accuracy, speed and robustness. We establish a baseline for the democratic honeybees algorithm and ascertain the most adequate parameters to assure the algorithms best performance. With the implementation, we see that when it comes to robustness, the algorithm is resistant against noise, and to an extent, against rogue agents. In order to achieve better performance, memory was implemented, so that each agent remembers its previous best quality score and will keep it, if a better one was not found in the current exploration phase. While using three visits for each agent with a neighborhood of two, we notice tolerance against rogue agents to a degree, most noticeably when the quality score advertised by rogue agents is further from the true best solution. However, increasing the number of visits allowed for each agent to perform, significantly decreases rogue agents influence in the swarm at the cost of accuracy, where an increase in the iterations to convergence is observed. In case of noise, we notice a trend increasing that slows the swarm when exploring for the global minima, while also slightly decreasing its accuracy.

Nevertheless, the performance of the swarm is influenced by many parameters. Intuitively, the first parameters investigated for performance and accuracy effects were the number of agents and search space, which were immediately proven to impact the search negatively or positively, respectively. Subsequently, the number of visits and neighborhood were inspected where the number of visits proved to have very low impact in the swarm, while number of neighbors improved its efficiency, simultaneously decreasing the needed amount of iterations. Moreover, the influence of the number of runs needed to achieve consensus was tested, where positive results were obtained in regard of the accuracy of the swarm whilst, as expected, using more iterations. When plotting said parameter against the threshold for the majority, we observe a similar trend where the swarm appears to consent to the true best solution more frequently with an increasing majority threshold. 
Lastly, the number of visits and neighborhood size were plotted against each other, to determine their effect. The swarm appears to react very well, to the increase of both parameters, increasing their accuracy finding the true best solution, even in large search spaces, already with a neighborhood of ten and 14 visits with an overall trend downwards. The same trend was observed in the iterations to convergence plot, where increasing both of those parameters, increased the overall speed of the algorithm to four iterations. 

For further investigations, one could use different functions, other than the Rastrigin, to test the parameters of the algorithm. It also would be interesting to build a dynamic small world topology, over the swarm, that would decrease the connected edges dynamically \cite{liu_dynamic_2016}. This idea combined with a network with trust edges\cite{rodriguez_smartocracy_2007}, where a rogue agent would have its trust reduced by the swarm when spamming bad positions, could improve the robustness of the swarm significantly. This hypothesis is based on the observation that our algorithm will never be robust against  a number of rogue agents that is larger than half of the total number of agents. With such an implementation, the robustness of the swarm, would become invariant to the number of rogue agents.

\bibliographystyle{IEEEtran}
\bibliography{IEEEexample.bib}

\end{document}
